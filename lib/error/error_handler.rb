module Error
  module ErrorHandler
    def self.included(clazz)
      clazz.class_eval do
        rescue_from Exception do |e|
          respond(:internal_server_error, e.message)
        end
        rescue_from ActiveRecord::RecordNotFound do |e|
          respond(:not_found, e.message)
        end
        rescue_from Stripe::CardError, Stripe::InvalidRequestError do |e|
          respond(:bad_request, e.message)
        end
      end
    end

    private

    def respond(_status, _message)
      render json: { error: _message }, status: _status
    end
  end
end