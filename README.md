### Running Locally

The application runs entirely in a Virtual Machine.

First, ensure that you have the following components installed:

1. [Install VirtualBox](https://releases.hashicorp.com/vagrant/2.0.1/vagrant_2.0.1_x86_64.dmg?_ga=2.162084412.1419666050.1511863190-1387584540.1505460054)
2. [Install Vagrant](http://download.virtualbox.org/virtualbox/5.2.2/VirtualBox-5.2.2-119230-OSX.dmg)
3. In your terminal, run `vagrant up` and wait for it to finish
4. Next, run `vagrant ssh` to SSH into the Virtual Machine
5. `cd` to your project code can be found in: `~/app`
6. Run `bundle exec rake db:migrate`
7. Run `bundle exec rake db:seed`
8. Run `rails s` to start the Rails server
9. Open your browser and navigate to: `http://localhost:3000` on your host machine
10. Use Git to commit on your host machine, not the VM

The virtual machine is **automatically configured** with Redis, PostgreSQL and any other dependencies you might need for the project via the `Vagrantfile`.

### Adding Runtime Environment Variables

If you need to add environment variables that must be available to the application, please do so by adding them to the `Vagrantfile`. DevOps will use these to re-create the correct values in production.

The format is as follows:

```
# Inside Vagrantfile:

echo 'VARIABLE_NAME="VALUE"' | sudo tee --append /etc/environment > /dev/null
```

### Controlling the Virtual Machine

Stop the machine:

`vagrant halt`

Start the machine:

`vagrant up`

Connect to the machine:

`vagrant ssh`

Destroy the machine:

`vagrant destroy`

Note: If your VM becomes corrupted, go ahead and destroy it and create another - this is quite common.


### Global Variables Required:

- STRIPE_SECRET_KEY
- STRIPE_PUBLISHABLE_KEY