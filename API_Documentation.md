Vyya API

Login:
------

`POST /api/v1/users/sign_in`

Parameters:
- user[email] (String, regular email address)
- user[password] (String, password)

Signup:
------

`POST /api/v1/users`

Parameters:
- user[email] (String, regular email address)
- user[password] (String, password)
- user[first_name] (String, first name)
- user[last_name] (String, last name)

Logout:
-------

`DELETE /api/v1/users`

Headers:
- X-User-Token (user token)
- X-User-Email (user email address)

Get List of Cards:
------------------

`GET /api/v1/cards`

Headers:
- X-User-Token (user token)
- X-User-Email (user email address)

Create Card:
------------

`POST /api/v1/cards`

Headers:
- X-User-Token (user token)
- X-User-Email (user email address)

Parameters:
- card_token (String, stripe token)

Delete Card:
------------

`DELETE /api/v1/cards/:id`

Headers:
- X-User-Token (user token)
- X-User-Email (user email address)

Parameters:
- id (String, stripe card id)

Set Card as default payment source:
-----------------------------------

`POST /api/v1/cards/:id/set_as_default`

Headers:
- X-User-Token (user token)
- X-User-Email (user email address)

Parameters:
- id (String, stripe card id)

Get all user transactions:
--------------------------

`GET /api/v1/transactions`

Headers:
- X-User-Token (user token)
- X-User-Email (user email address)


Create Transaction:
-------------------

`POST /api/v1/transactions`

Headers:
- X-User-Token (user token)
- X-User-Email (user email address)

Parameters:
- merchant_id (integer, merchant id)
- amount (decimal, amount of transaction)

Status Codes:
200 - Success
204 - No Content
404 - Resource could not be found
401 - Unauthorized
500 - Error

If the 404 and 500 status codes are present, the JSON payload will also contain
an *errors* array field with the server error messages.