Rails.application.routes.draw do
  devise_for :users, only: []
  devise_for :admins, path: 'dashboard', only: %i[sessions unlock]


  namespace :dashboard do
    get '/', to: 'index#index'
    resources :merchants do
      member do
        get 'transactions', action: :transactions
        get 'download_qrcode', action: :download_qrcode
      end
    end
  end

  root 'dashboard/index#index'

  namespace :api, path: 'api/(v:api)', defaults: { format: 'json' } do
    devise_scope :user do
      post '/users' => '/users/registrations#create'
      delete '/users/sign_out' => '/users/sessions#destroy'
      post '/users/sign_in' => '/users/sessions#create'
    end
    resources :cards, only: [:index, :create, :destroy] do
      member do
        post '/set_as_default', action: :set_as_default
      end
    end
    resources :transactions, only: [:index, :create]
  end
end
