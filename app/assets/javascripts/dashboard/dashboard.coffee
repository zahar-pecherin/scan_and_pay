dashboard =
  init: ->
    do @submit_new_edit_form
    do @ptint_qrcode

  submit_new_edit_form: ->
    $('.submit-form-button').click (e) ->
      e.preventDefault();
      target = $(@).attr('data-target')
      $("#form-#{target}").submit()

  print_div: ->
    console.log('Here')
    divToPrint = document.getElementById('div-to-print');
    popupWin = window.open('', '_blank', 'width=530,height=530');
    popupWin.document.open();
    popupWin.document.write('<html><body onload="window.print()"><style type="text/css" media="all">table {border-spacing:0px; -webkit-print-color-adjust:exact;} .black, .white {width:10px;height:10px;padding:0px} .black {background-color:black !important;} .white {background-color:white !important;}</style>' + divToPrint.innerHTML + '</body></html>');
    popupWin.document.close();

  ptint_qrcode: ->
    $('.print-qrcode').click (e) ->
      e.preventDefault()
      dashboard.print_div()

$(document).on 'turbolinks:load', ->
  do dashboard.init