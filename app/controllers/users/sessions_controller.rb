class Users::SessionsController < Devise::SessionsController
  respond_to :json

  skip_before_filter :verify_signed_out_user
  acts_as_token_authentication_handler_for User
  skip_before_action :authenticate_user_from_token!, only: :create

  # DELETE /resource/sign_out
  def destroy
    user = User.find_by_authentication_token_and_email(params[:user_token], params[:user_email])

    if user
      user.authentication_token = nil
      user.save!
    else
      return render json: { errors: 'Invalid Token'}, status: :unauthorized
    end
    super
  end

  private

  def resource_params
    params.require(:user).permit(:email, :password)
  end
end
