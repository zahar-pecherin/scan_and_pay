class Users::RegistrationsController < Devise::RegistrationsController
  respond_to :json

  protected

  def sign_up_params
    params.require(:user).permit(:email, :password, :first_name, :last_name, :password_confirmation)
  end
end
