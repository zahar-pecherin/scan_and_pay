class Dashboard::MerchantsController < Dashboard::ApplicationController
  before_action :set_merchant, only: [:show, :edit, :update, :destroy, :transactions, :download_qrcode]

  # GET /merchants
  # GET /merchants.json
  def index
    @merchants = Merchant.all
  end

  # GET /merchants/1
  # GET /merchants/1.json
  def show
    redirect_to edit_dashboard_merchant_path(@merchant)
  end

  # GET /merchants/new
  def new
    @merchant = Merchant.new
  end

  # GET /merchants/1/edit
  def edit
    @merchant = Merchant.find(params[:id])
  end

  # POST /merchants
  # POST /merchants.json
  def create
    @merchant = Merchant.new(merchant_params)

    if @merchant.save
      redirect_to edit_dashboard_merchant_path(@merchant), notice: 'Merchant was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /merchants/1
  # PATCH/PUT /merchants/1.json
  def update
    respond_to do |format|
      if @merchant.update(merchant_params)
        format.html { redirect_to dashboard_merchant_url(@merchant), notice: 'Merchant was successfully updated.' }
        format.json { render :show, status: :ok, location: @merchant }
      else
        format.html { render :edit }
        format.json { render json: @merchant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /merchants/1
  # DELETE /merchants/1.json
  def destroy
    @merchant.destroy
    redirect_to dashboard_merchants_url, notice: 'Merchant was successfully destroyed.'
  end

  def transactions
    @transactions = @merchant.transactions.joins(:user)
  end

  def download_qrcode
    qrcode = RQRCode::QRCode.new(@merchant.as_json(only: %i[id name email contact_number address]).to_json).as_png(size: 400)
    send_data qrcode, type: 'image/png'
  end

  private

  def set_merchant
    @merchant = Merchant.find(params[:id])
  end

  def merchant_params
    params.require(:merchant).permit(:name, :email, :vat_number, :contact_number, :address)
  end
end
