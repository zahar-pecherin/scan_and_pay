class Dashboard::IndexController  < Dashboard::ApplicationController
  def index
    redirect_to dashboard_merchants_path
  end
end
