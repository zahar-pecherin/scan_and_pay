class Api::TransactionsController < Api::BaseController
  def index
    transactions = current_user.transactions.eager_load(:merchant)
    render json: { transactions: transactions.map { |transaction| transaction.as_json.merge(merchant: transaction.merchant.as_json(only: %i[name email contact_number address amount])) } }
  end

  def create
    merchant = Merchant.find_by_id(params[:merchant_id])
    return render json: { error: 'Merchant not found' }, status: :not_found unless merchant

    amount = params[:amount].to_f
    return render json: { error: 'Amount must be greater than 0' }, status: :unprocessable_entity if amount.zero?

    transaction = Stripe::Charge.create(
      amount: (amount  * 100).to_i,
      currency: 'usd',
      customer: current_user.customer_id,
      :description => "Charge for #{merchant.name} (#{merchant.id}) #{merchant.email}"
    )
    current_user.transactions.create(merchant_id: merchant.id, charge_id: transaction.id, amount: transaction.amount.to_f / 100)
    render json: { message: 'Success' }
  end
end