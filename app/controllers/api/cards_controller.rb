class Api::CardsController < Api::BaseController

  def index
    return render(json: { cards: [] }) unless current_user.customer_id
    customer = get_customer
    render json: { cards: customer.sources.all(object: 'card')['data'].map { |card| card.as_json.merge(active_card: card.id.eql?(customer.default_source)) } }
  end

  def create
    customer = stripe_customer
    customer.sources.create(source: params[:card_token])
    render json: { message: 'Success' }
  end

  def destroy
    customer = get_customer
    customer.sources.retrieve(params[:id]).delete
    render json: { message: 'Success' }
  end

  def set_as_default
    customer = get_customer
    customer.default_source = params[:id]
    customer.save
    render json: { message: 'Success' }
  end

  private

  def stripe_customer
    current_user.customer_id ? get_customer : create_customer
  end

  def get_customer
    Stripe::Customer.retrieve(current_user.customer_id)
  end

  def create_customer
    customer = Stripe::Customer.create(email: current_user.email, description: "Customer app_id #{current_user.id}")
    current_user.update(customer_id: customer.id)
    customer
  end
end