class Api::BaseController < ActionController::Base
  include Error::ErrorHandler

  protect_from_forgery prepend: true

  acts_as_token_authentication_handler_for User

  respond_to :json
end
