class Merchant < ApplicationRecord
  validates :name, :email, :vat_number, :contact_number, :address, presence: true
  validates_format_of :email, with: /\A[^@\s]+@[^@\s]+\z/, allow_blank: true

  has_many :transactions, -> { order(created_at: :desc)}, dependent: :destroy

  after_save :add_qrcode

  private

  def add_qrcode
    qrcode = RQRCode::QRCode.new(self.as_json(only: %i[id name email contact_number address]).to_json)
    self.update_column(:qrcode, qrcode.as_html)
  end
end
