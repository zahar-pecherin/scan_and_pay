class Transaction < ApplicationRecord
  belongs_to :user
  belongs_to :merchant

  validates :charge_id, presence: true
end
