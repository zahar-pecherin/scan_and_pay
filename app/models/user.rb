class User < ApplicationRecord
  acts_as_token_authenticatable

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :transactions, -> { order(created_at: :desc)}, dependent: :destroy

  validates :first_name, :last_name, presence: true
end
