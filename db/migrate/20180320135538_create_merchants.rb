class CreateMerchants < ActiveRecord::Migration[5.0]
  def change
    create_table :merchants do |t|
      t.string :name
      t.string :email
      t.string :vat_number
      t.string :contact_number
      t.string :address
      t.string :qrcode

      t.timestamps
    end
  end
end
