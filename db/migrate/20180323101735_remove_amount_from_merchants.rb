class RemoveAmountFromMerchants < ActiveRecord::Migration[5.0]
  def change
    remove_column :merchants, :amount, :decimal
  end
end
