class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions do |t|
      t.integer :user_id
      t.integer :merchant_id
      t.string :charge_id
      t.decimal :amount

      t.timestamps
    end
  end
end
