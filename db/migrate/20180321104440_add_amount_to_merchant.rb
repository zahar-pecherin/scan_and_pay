class AddAmountToMerchant < ActiveRecord::Migration[5.0]
  def change
    add_column :merchants, :amount, :decimal
  end
end
